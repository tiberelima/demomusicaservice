package com.tlima.demomusicaservice;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.tlima.demomusicaservice.service.Musica;
import com.tlima.demomusicaservice.service.MusicaService;
import com.tlima.demomusicaservice.service.MusicaServiceConnection;

import java.util.Arrays;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    private Button btTocar;
    private Button btPausar;
    private Button btParar;
    private MusicaServiceConnection connection;
    private Intent intent;
    private Musica musica;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            List<Musica> musicas = Arrays.asList(
                    new Musica(1, "musica01.mp3", "Can't Stop Lovin' You", "Van Halen", R.mipmap.ic_musica01),
                    new Musica(2, "musica02.mp3", "Bark At The Moon", "Ozzy Osbourne", R.mipmap.ic_musica02),
                    new Musica(3, "musica03.mp3", "Maxwell Murder", "Rancid", R.mipmap.ic_musica03)
            );

            iniciaServico();
            conectaServico();

            int position = extras.getInt("id");
            musica = musicas.get(position);

            ImageView folder = (ImageView) findViewById(R.id.folder);
            TextView nome = (TextView) findViewById(R.id.nome);
            TextView autor = (TextView) findViewById(R.id.autor);

            folder.setImageResource(musica.getFolder());
            nome.setText(musica.getNome());
            autor.setText(musica.getAutor());

            btTocar = (Button) findViewById(R.id.btTocar);
            btPausar = (Button) findViewById(R.id.btPausar);
            btParar = (Button) findViewById(R.id.btParar);
        }
    }

    private void iniciaServico() {
        intent = new Intent(this, MusicaService.class);
        startService(intent);
    }

    private void conectaServico() {
        connection = new MusicaServiceConnection();
        bindService(intent, connection, 0);
    }

    public void tocar(View view) {
        connection.getMusicaService().tocar(musica);
        ativarDesativarBotoes(true);
    }

    public void pausar(View view) {
        connection.getMusicaService().pausar();
        ativarDesativarBotoes(false);
    }

    public void parar(View view) {
        connection.getMusicaService().parar();
        ativarDesativarBotoes(false);
    }

    private void ativarDesativarBotoes(boolean estaTocando) {
        btTocar.setEnabled(!estaTocando);
        btPausar.setEnabled(estaTocando);
        btParar.setEnabled(estaTocando);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(connection);
    }
}

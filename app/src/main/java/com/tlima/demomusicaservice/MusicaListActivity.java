package com.tlima.demomusicaservice;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.tlima.demomusicaservice.adapter.ListMusicaAdatper;
import com.tlima.demomusicaservice.service.Musica;

import java.util.Arrays;
import java.util.List;

public class MusicaListActivity extends ActionBarActivity {

    private ListView musicaList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_musica_list);

        List<Musica> musicas = Arrays.asList(
                new Musica(1, "musica01.mp3", "Can't Stop Lovin' You", "Van Halen", R.mipmap.ic_musica01),
                new Musica(2, "musica02.mp3", "Bark At The Moon", "Ozzy Osbourne", R.mipmap.ic_musica02),
                new Musica(3, "musica03.mp3", "Maxwell Murder", "Rancid", R.mipmap.ic_musica03)
        );


        musicaList = (ListView) findViewById(R.id.musica_list);
        musicaList.setAdapter(new ListMusicaAdatper(this, musicas));
        musicaList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                startActivity(new Intent(getApplicationContext(), MainActivity.class).putExtra("id", position));
            }
        });
    }
}

package com.tlima.demomusicaservice;

import android.app.Service;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.IBinder;

import com.tlima.demomusicaservice.binder.MusicaBinder;

public class MusicaService extends Service {

    private MediaPlayer mediaPlayer;
    //posicao atual da musica em milisegundos
    private int posicao = 0;
    private static final String MUSICA = "";
    private IBinder binder = new MusicaBinder(this);

    public MusicaService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public void tocar() {
        if (posicao > 0) {
            mediaPlayer.seekTo(posicao);
        } else {
            mediaPlayer = new MediaPlayer();
            try {
                AssetFileDescriptor afd = getApplicationContext().getAssets().openFd(MUSICA);
                mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                mediaPlayer.prepare();
            } catch (Exception e) {

            }
        }
        mediaPlayer.start();
    }
}

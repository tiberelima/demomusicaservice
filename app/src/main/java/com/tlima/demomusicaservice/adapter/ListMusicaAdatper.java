package com.tlima.demomusicaservice.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tlima.demomusicaservice.R;
import com.tlima.demomusicaservice.service.Musica;

import java.util.List;

public class ListMusicaAdatper extends BaseAdapter {

    private final Context context;
    private final List<Musica> musicas;

    public ListMusicaAdatper(Context context, List<Musica> musicas) {
        this.context = context;
        this.musicas = musicas;
    }

    @Override
    public int getCount() {
        return musicas.size();
    }

    @Override
    public Object getItem(int position) {
        return musicas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return musicas.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.musica, parent, false);
        }

        Musica musica = musicas.get(position);

        TextView firstLine = (TextView) convertView.findViewById(R.id.firstLine);
        TextView secondLine = (TextView) convertView.findViewById(R.id.secondLine);
        ImageView folder = (ImageView) convertView.findViewById(R.id.icon);

        firstLine.setText(musica.getNome());
        secondLine.setText(musica.getAutor());
        folder.setImageResource(musica.getFolder());

        return convertView;
    }
}

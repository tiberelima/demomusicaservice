package com.tlima.demomusicaservice.binder;

import android.os.Binder;

import com.tlima.demomusicaservice.service.MusicaService;

public class MusicaBinder extends Binder {

    private MusicaService musicaService;

    public MusicaBinder(MusicaService musicaService) {
        this.musicaService = musicaService;
    }

    public MusicaService getMusicaService() {
        return this.musicaService;
    }
}

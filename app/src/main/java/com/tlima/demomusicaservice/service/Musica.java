package com.tlima.demomusicaservice.service;

public class Musica {

    private final int id;
    private final String faixa;
    private final String nome;
    private final String autor;
    private final int folder;

    public Musica(int id, String faixa, String nome, String autor, int folder) {
        this.id = id;
        this.faixa = faixa;
        this.nome = nome;
        this.autor = autor;
        this.folder = folder;
    }

    public int getId() {
        return id;
    }

    public String getFaixa() {
        return faixa;
    }

    public String getNome() {
        return nome;
    }

    public String getAutor() {
        return autor;
    }

    public int getFolder() {
        return folder;
    }
}
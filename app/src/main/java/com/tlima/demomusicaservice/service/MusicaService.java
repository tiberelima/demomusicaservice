package com.tlima.demomusicaservice.service;

import android.app.Service;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.annotation.NonNull;

import com.tlima.demomusicaservice.binder.MusicaBinder;

public class MusicaService extends Service {

    private MediaPlayer mediaPlayer;
    private int posicao = 0;
    private IBinder binder = new MusicaBinder(this);
    private Musica musica;


    public MusicaService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public void tocar(@NonNull Musica musica) {

        if (!musica.equals(this.musica)) {
            this.musica = musica;
            parar();
        }

        if (posicao > 0) {
            mediaPlayer.seekTo(posicao);
        } else {
            mediaPlayer = new MediaPlayer();
            try {
                AssetFileDescriptor afd = getApplicationContext().getAssets().openFd(musica.getFaixa());
                mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                mediaPlayer.prepare();
            } catch (Exception e) {
            }
        }
        mediaPlayer.start();
    }

    public void parar() {
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            }
        }
        posicao = 0;
    }

    public void pausar() {
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
                posicao = mediaPlayer.getCurrentPosition();
            }
        }
    }
}

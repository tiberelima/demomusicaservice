package com.tlima.demomusicaservice.service;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.tlima.demomusicaservice.binder.MusicaBinder;

public class MusicaServiceConnection implements ServiceConnection {

    private MusicaService musicaService;

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        MusicaBinder musicaBinder = (MusicaBinder) service;
        this.musicaService = musicaBinder.getMusicaService();
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {

    }

    public MusicaService getMusicaService() {
        return musicaService;
    }
}
